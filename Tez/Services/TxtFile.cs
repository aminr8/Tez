﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public static class TxtFile
    {
        public static List<string> LoadTxtFile(string path)
        {
            try
            {
                var ans = new List<string>();
                var line = "";
                var file = new System.IO.StreamReader(path);
                while ((line = file.ReadLine()) != null)
                    ans.Add(line);

                file.Close();
                return ans;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public static string LoadTxtFileOnOneString(string path)
        {
            try
            {
                var ans = "";
                var line = "";
                var file = new System.IO.StreamReader(path);
                while ((line = file.ReadLine()) != null)
                    ans += ("\n" + line);

                file.Close();
                return ans;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public static bool SaveTxtFile(string path, string temp)
        {
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path);
                file.WriteLine(temp);

                file.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public static List<string> SplitString(string line)
        {
            try
            {
                var ans = line.Split(' ').ToList();
                return ans;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        } 
    }
}
