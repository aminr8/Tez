﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.FileReader
{
    public static class TxtReader
    {
        public static List<List<string>> ReadFile(string url)
        {
            try
            {
                var ans = new List<List<string>>();
                string line;
                var file = new System.IO.StreamReader(url);
                while ((line = file.ReadLine()) != null)
                {
                    var a = line.Split(' ').ToList();
                    ans.Add(a);
                }
                file.Close();
                return ans;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public static bool WriteFile(List<string> list , string url)
        {
            try
            {
                var lines = list.Aggregate("", (current, temp) => current + (temp + "\n"));

                var file = new System.IO.StreamWriter(url);
                file.WriteLine(lines);

                file.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }
    }
}
