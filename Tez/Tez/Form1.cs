﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DAL.DbModel;
using DAL.Repository;
using DAL.Repository.Sql;
using Services;

namespace Tez
{
    public partial class Form1 : Form
    {

        public IRepo Repo;

        public Form1()
        {
            InitializeComponent();
        }

        public static class ThreadSafeRandom
        {
            [ThreadStatic] private static Random _local;

            public static Random ThisThreadsRandom
            {
                get
                {
                    return _local ??
                           (_local =
                               new Random(unchecked(Environment.TickCount*31 + Thread.CurrentThread.ManagedThreadId)));
                }
            }
        }

        public static void Shuffle<T>(IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        private void AddProject_Click(object sender, EventArgs e)
        {
            var project = new Project();
            Repo = new Repo();
            project.IsLoad = false;
            project.Link = "http://codeforces.com/problemset/problem/618/G";
            project.Name = "G. Combining Slimes";
            project.ParametrCnt = 2;
            var ans = Repo.AddProject(project);
            MessageBox.Show(ans.ToString());
        }

        private void AddCodeProject_Click(object sender, EventArgs e)
        {
            var codeProject = new CodeProject();
            Repo = new Repo();
            codeProject.Project_Id = 1;
            codeProject.ProjectCode =
                TxtFile.LoadTxtFileOnOneString(@"C:\Users\karimi\OneDrive\Sample Code For Test\Combining Slimes.h");
            var ans = Repo.AddCodeProject(codeProject);
            MessageBox.Show(ans.ToString());
        }

        private void AddInputProject_Click(object sender, EventArgs e)
        {
            Repo = new Repo();
            var lines = TxtFile.LoadTxtFile
                (@"C:\Users\a.karimi\OneDrive\Uni\Tez\Cpp_Generator\Cpp_Generator\Cpp_Generator\Combining_Slimes10000.txt");
            var ans = true;
            foreach (var temp in lines)
            {
                var t = TxtFile.SplitString(temp);
                var input = new Input();
                var inputValue = new List<InputValue>();
                input.InputNumber = t.Count() - 2;
                input.Project_Id = 1;
                input.Result = double.Parse(t[t.Count - 2]);
                input.NodePath = t[t.Count - 1];
                for (var i = 0; i < t.Count - 2; i++)
                {
                    var value = new InputValue();
                    value.Value = long.Parse(t[i]);
                    value.ParametrNamber = i;
                    //value.NodePath = 
                    inputValue.Add(value);
                }
                ans &= Repo.AddInput(input, inputValue);
            }
            MessageBox.Show(ans.ToString());
        }

        private void AddProjectNodes_Click(object sender, EventArgs e)
        {
            Repo = new Repo();
            var lines = TxtFile.LoadTxtFile(@"C:\Users\a.karimi\OneDrive\Uni\Tez\Combining_Slimes\Nodes.txt");
            var ans = true;
            foreach (var temp in lines)
            {
                var t = TxtFile.SplitString(temp);
                if (t == null || t.Count < 2)
                    continue;
                var nodeProject = new NodeProject();
                nodeProject.Project_Id = 1;
                nodeProject.NodeState_Id = long.Parse(t[1]);
                nodeProject.NodeNumber = int.Parse(t[0]);
                nodeProject.Code = "";
                ans &= Repo.AddNodes(nodeProject);
            }
            MessageBox.Show(ans.ToString());

        }

        private void AddEdgeNode_Click(object sender, EventArgs e)
        {
            Repo = new Repo();
            var lines = TxtFile.LoadTxtFile(@"C:\Users\a.karimi\OneDrive\Uni\Tez\Combining_Slimes\Edges.txt");
            var projectId = 1;
            var ans = true;
            foreach (var temp in lines)
            {
                var t = TxtFile.SplitString(temp);
                if (t == null || t.Count < 2)
                    continue;
                var edgeNode = new EdgeNode();
                edgeNode.FirstNode_Id =
                    Repo.GetIdOfNodeByProjectIdAndNodeNumber(projectId, int.Parse(t[0]));
                edgeNode.SecondNode_Id =
                    Repo.GetIdOfNodeByProjectIdAndNodeNumber(projectId, int.Parse(t[1]));
                ans &= Repo.AddEdgeNode(edgeNode);
            }
            MessageBox.Show(ans.ToString());

        }

        private void AddTSK_Click(object sender, EventArgs e)
        {
            Repo = new Repo();
            var tsk = new TSK();
            tsk.GridNumber = 2;
            tsk.InputNumber = 20;
            tsk.Project_Id = 1;
            Repo.AddTsk(tsk);
            var allInput = Repo.GetAllInputByProjectId(tsk.Project_Id);
            if (allInput == null || allInput.Count == 0)
                return;
            Shuffle(allInput);
            allInput = allInput.Take(tsk.InputNumber).ToList();
            foreach (var temp in allInput)
            {
                var tskInput = new TskInput();
                tskInput.Input_Id = temp.Id;
                tskInput.TSK_Id = tsk.Id;
                Repo.AddTskInput(tskInput);
            }
        }

        private void AddTskGrid_Click(object sender, EventArgs e)
        {
            Repo = new Repo();
            const int tskId = 2;
            var tsk = Repo.FindTskbyId(tskId);
            var project = Repo.FindProjectByTskId(tskId);
            for (var i = 0; i < project.ParametrCnt; i++)
            {
                var inputValue = Repo.GetAllInputValueByFixParametrAndTskId
                    (i, tsk.Id).OrderBy(x => x.Value).ToList();

                var step = inputValue.Count/tsk.GridNumber;
                var index = 0;
                for (var j = 0; j < tsk.GridNumber; j++)
                {
                    var tskGrid = new TSKGrid();
                    tskGrid.DimensionNumber = i;
                    tskGrid.EndValue =
                        inputValue[Math.Min(index + step, inputValue.Count - 1)].Value;
                    tskGrid.TSK_Id = tsk.Id;
                    tskGrid.StartValue = inputValue[index].Value;
                    index += (step + 1);
                    Repo.AddTskGrid(tskGrid);
                }
            }

        }

        private void AddAnswerToGrid_Click(object sender, EventArgs e)
        {
            Repo = new Repo();
            const int tskId = 2;
            var inputs = Repo.GetAllTskInputByTskId(tskId);
            var cells = Repo.GetAllTskCells(tskId);
            foreach (var t in inputs)
            {
                foreach (var cell in cells)
                {
                    var can = InputHasInCell(t.Input, cell);
                    if (can)
                    {
                        var a = new TskInputInCell();
                        a.TskCell_Id = cell.Id;
                        a.TskInput_Id = t.Input_Id;
                        Repo.AddTskInputInCell(a);
                        break;
                    }
                }
            }

            foreach (var t in cells)
            {
                var ins = Repo.GetAllInputInCellsByCellId(t.Id);
                foreach (var temp in ins)
                {
                    var an = Repo.GeTskGridAnswerByTskCellIdAndNodePath(t.Id, temp.TskInput.Input.NodePath);
                    if (an == null)
                    {
                        var answer = new TSKGridAnswer();
                        answer.NodePath = temp.TskInput.Input.NodePath;
                        answer.Probability = 1;
                        answer.Count = ins.Count;
                        answer.TskCell_Id = t.Id;
                        Repo.AddOrUpdateTskGridAnswer(answer);
                    }
                    else
                    {
                        an.Probability++;
                        Repo.AddOrUpdateTskGridAnswer(an);
                    }
                }
            }

        }

        private void CreateCell_Click(object sender, EventArgs e)
        {
            Repo = new Repo();
            const int tskId = 2;
            const int bod = 3;
            var tskGrid = Repo.GetAllTskGridsByTskId(tskId);
            long[] arr;
            arr = new long[bod];
            CreateCell(0, tskGrid, 0, arr);

        }



        private bool InputHasInCell(Input input, TskCell tskCell)
        {
            try
            {
                var tskGridCells = Repo.GetAllTskGridCellsByTskCellId(tskCell.Id);
                var inputValues = Repo.GetAllInputValueByInputId(input.Id);
                foreach (var t in tskGridCells)
                {
                    var grid = t.TSKGrid;
                    var inputVal = inputValues.First(x => x.ParametrNamber == grid.DimensionNumber);
                    if (grid.StartValue > inputVal.Value || grid.EndValue < inputVal.Value)
                        return false;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        private int  CreateCell(int level, List<TSKGrid> list , int cellNumber , long [] arr)
        {
            const int tskId = 2;
            var t = list.Where(x => x.DimensionNumber == level).ToList();
            if (t == null || t.Count == 0)
            {
                var cell = new TskCell();
                cell.Tsk_Id = tskId;
                cell.GridCell = cellNumber;
                Repo.AddTskCell(cell);
                for (int i = 0; i < level; i++)
                {
                    var gridCell = new TskGridCell();
                    gridCell.TskCell_Id = cell.Id;
                    gridCell.TskGrid_Id = arr[i];
                    Repo.AddTskCellGrid(gridCell);
                }
                cellNumber ++;
            }
            else
            {
                foreach (var temp in t)
                {
                    arr[level] = temp.Id;
                    cellNumber = CreateCell(level + 1, list, cellNumber, arr);
                }
            }
            return cellNumber;
        }
    }
}
 