//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.DbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class TskInput
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TskInput()
        {
            this.TskInputInCells = new HashSet<TskInputInCell>();
        }
    
        public long Id { get; set; }
        public long TSK_Id { get; set; }
        public long Input_Id { get; set; }
    
        public virtual Input Input { get; set; }
        public virtual TSK TSK { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TskInputInCell> TskInputInCells { get; set; }
    }
}
