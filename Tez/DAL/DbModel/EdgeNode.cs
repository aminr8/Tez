//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.DbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class EdgeNode
    {
        public long Id { get; set; }
        public long FirstNode_Id { get; set; }
        public long SecondNode_Id { get; set; }
    
        public virtual NodeProject NodeProject { get; set; }
        public virtual NodeProject NodeProject1 { get; set; }
    }
}
