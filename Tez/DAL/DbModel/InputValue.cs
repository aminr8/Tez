//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.DbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class InputValue
    {
        public long Id { get; set; }
        public long Input_Id { get; set; }
        public int ParametrNamber { get; set; }
        public long Value { get; set; }
    
        public virtual Input Input { get; set; }
    }
}
