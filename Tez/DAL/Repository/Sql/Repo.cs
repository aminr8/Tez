﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using DAL.DbModel;

namespace DAL.Repository.Sql
{
    public class Repo : IRepo
    {
        public InputGeneratorTSKEntities Context;

        public Repo()
        {
            Context = new InputGeneratorTSKEntities();
        }

        public bool AddProject(Project project)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                Context.Projects.AddOrUpdate(project);
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AddCodeProject(CodeProject codeProject)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                Context.CodeProjects.AddOrUpdate(codeProject);
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public bool AddInput(Input input, List<InputValue> list)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                Context.Inputs.AddOrUpdate(input);
                Context.SaveChanges();
                foreach (var temp in list)
                {
                    temp.Input_Id = input.Id;
                    Context.InputValues.AddOrUpdate(temp);
                    Context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public bool AddNodes(NodeProject nodeProject)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                Context.NodeProjects.Add(nodeProject);
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public bool AddEdgeNode(EdgeNode edgeNode)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                Context.EdgeNodes.AddOrUpdate(edgeNode);
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public List<Input> GetAllInputByProjectId(long projectId)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                var ans = Context.Inputs.Where(x => x.Project_Id == projectId).ToList();
                return ans;
            }
            catch (Exception ex)
            {
                //return null;
                throw;
            }
        }

        public bool AddTsk(TSK tsk)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                Context.TSKs.AddOrUpdate(tsk);
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public bool AddTskInput(TskInput tskInput)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                Context.TskInputs.AddOrUpdate(tskInput);
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public List<TskInput> GetAllTskInputByTskId(long tskId)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                var ans = Context.TskInputs.Where(x => x.TSK_Id == tskId).ToList();
                return ans;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public Project FindProjectByTskId(long tskId)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                var ans = Context.TSKs.FirstOrDefault(x => x.Id == tskId);
                return ans?.Project;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public TSK FindTskbyId(long tskId)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                var ans = Context.TSKs.FirstOrDefault(x => x.Id == tskId);
                return ans;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public List<InputValue> GetAllInputValueByInputId(long inputId)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                var ans = Context.InputValues.Where(x => x.Input_Id == inputId).ToList();
                return ans;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public List<InputValue> GetAllInputValueByFixParametrAndTskId(int parametIndex, long tskId)
        {
            Context = new InputGeneratorTSKEntities();
            try
            {
                var ans = new List<InputValue>();
                var data = (from tskInput in Context.TskInputs
                        .Where(x => x.TSK_Id == tskId)
                    join input in Context.Inputs
                    on tskInput.Input_Id equals input.Id
                    join inputValue in Context.InputValues
                        .Where(x => x.ParametrNamber == parametIndex)
                    on input.Id equals inputValue.Input_Id
                    select new
                    {
                        Id = inputValue.Id,
                        Input_Id = inputValue.Input_Id,
                        ParametrNamber = inputValue.ParametrNamber,
                        Value = inputValue.Value
                    }).ToList();
                //ans = data.Cast<InputValue>().ToList();
                foreach (var inputValue in data)
                {
                    var t = new InputValue();
                    t.Id = inputValue.Id;
                    t.Input_Id = inputValue.Input_Id;
                    t.ParametrNamber = inputValue.ParametrNamber;
                    t.Value = inputValue.Value;
                    ans.Add(t);
                }
                return ans;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public bool AddTskGrid(TSKGrid tskGrid)
        {
            try
            {
                Context.TSKGrids.AddOrUpdate(tskGrid);
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public List<InputValue> GetInputValueByInputId(long inputId)
        {
            try
            {
                var ans = Context.InputValues.Where(x => x.Input_Id == inputId)
                    .OrderBy(x => x.ParametrNamber).ToList();
                return ans;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public long GetIdOfNodeByProjectIdAndNodeNumber(long id, int num)
        {
            try
            {
                var ans = Context.NodeProjects.FirstOrDefault(x => x.Project_Id == id && x.NodeNumber == num);
                if (ans == null)
                    return -2;
                return ans.Id;
            }
            catch (Exception ex)
            {
                return -1;
                throw;
            }
        }

        public List<TSKGrid> GetAllTskGridsByTskId(long tskId)
        {
            try
            {
                var ans = Context.TSKGrids.Where(x => x.TSK_Id == tskId).ToList();
                return ans;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public bool AddTskCell(TskCell cell)
        {
            try
            {
                Context.TskCells.AddOrUpdate(cell);
                Context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool AddTskCellGrid(TskGridCell cell)
        {
            try
            {
                Context.TskGridCells.AddOrUpdate(cell);
                Context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public List<TskCell> GetAllTskCells(long tskId)
        {
            try
            {
                var ans = Context.TskCells.Where(x => x.Tsk_Id == tskId).ToList();
                return ans;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public List<TskGridCell> GetAllTskGridCellsByTskCellId(long tskCellId)
        {
            try
            {
                var ans = Context.TskGridCells.Where(x => x.TskCell_Id == tskCellId).ToList();
                return ans;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public bool AddTskInputInCell(TskInputInCell cell)
        {
            try
            {
                Context.TskInputInCells.AddOrUpdate(cell);
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public List<TskInputInCell> GetAllInputInCellsByCellId(long cellId)
        {
            try
            {
                var ans = Context.TskInputInCells.Where(x => x.TskCell_Id == cellId).ToList();
                return ans;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public TSKGridAnswer GeTskGridAnswerByTskCellIdAndNodePath(long tskCellId, string nodePath)
        {
            try
            {
                var ans =
                    Context.TSKGridAnswers
                        .FirstOrDefault(x => x.NodePath == nodePath && x.TskCell_Id == tskCellId);
                return ans;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public bool AddOrUpdateTskGridAnswer(TSKGridAnswer answer)
        {
            try
            {
                Context.TSKGridAnswers.AddOrUpdate(answer);
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }


    }
}
