﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.DbModel;

namespace DAL.Repository
{
    public interface IRepo
    {
        bool AddProject(Project project);
        bool AddCodeProject(CodeProject codeProject);
        bool AddInput(Input input, List<InputValue> list);
        bool AddNodes(NodeProject nodeProject);
        bool AddEdgeNode(EdgeNode edgeNode);
        List<Input> GetAllInputByProjectId(long projectId);
        bool AddTsk(TSK tsk);
        bool AddTskInput(TskInput tskInput);
        List<TskInput> GetAllTskInputByTskId(long tskId);
        Project FindProjectByTskId(long tskId);
        TSK FindTskbyId(long tskId);
        List<InputValue> GetInputValueByInputId(long inputId);
        List<InputValue> GetAllInputValueByFixParametrAndTskId(int parametIndex , long tskId);
        bool AddTskGrid(TSKGrid tskGrid);
        long GetIdOfNodeByProjectIdAndNodeNumber(long id, int num);
        List<TSKGrid> GetAllTskGridsByTskId(long tskId);
        bool AddTskCell(TskCell cell);
        bool AddTskCellGrid(TskGridCell cell);
        List<TskCell> GetAllTskCells(long tskId);
        List<TskGridCell> GetAllTskGridCellsByTskCellId(long tskGridId);
        List<InputValue> GetAllInputValueByInputId(long inputId);
        bool AddTskInputInCell(TskInputInCell cell);
        List<TskInputInCell> GetAllInputInCellsByCellId(long cellId);
        TSKGridAnswer GeTskGridAnswerByTskCellIdAndNodePath(long tskCellId, string nodePath);
        bool AddOrUpdateTskGridAnswer(TSKGridAnswer answer);

    }
}
